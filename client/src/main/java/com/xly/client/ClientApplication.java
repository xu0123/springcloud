package com.xly.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeMap;
import java.util.Vector;

@SpringBootApplication
@EnableDiscoveryClient
public class ClientApplication {
	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}

}
